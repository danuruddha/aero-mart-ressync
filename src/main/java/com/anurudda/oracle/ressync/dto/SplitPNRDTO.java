package com.anurudda.oracle.ressync.dto;

import lombok.Data;

import java.util.Date;

@Data
public class SplitPNRDTO {
    private String parentPNR;
    private String childPNR;
    private Date splitDate;
    private String carrierCode;
}
