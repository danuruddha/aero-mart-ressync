package com.anurudda.oracle.ressync.dto;

import java.text.SimpleDateFormat;
import java.util.Date;

public class CommonUtil {
    public static final String DATE_FORMAT = "dd-MMM-yyyy HH:mm:ss";
    public static final String DATE_FMT_ORACLE = "dd-Mon-yyyy HH24:mi:ss";
    public static final String[] ALL_CARRIER_CODES = new String[]{"G9","E5","3L","3O"};

    public static String formatDate(Date date){
        SimpleDateFormat sdf = new SimpleDateFormat(CommonUtil.DATE_FORMAT);
        return sdf.format(date);
    }
}
