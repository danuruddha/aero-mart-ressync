package com.anurudda.oracle.ressync.dto;

import java.util.Map;
import java.util.Set;

public class ReservationLot {
    public CarrierTuple carrierTuple;
    public Set<String> pnrs;
    public String fromDate;
    public String toDate;
    private final Map<String, Set<String>> pnrTnxIds;

    public ReservationLot(CarrierTuple carrierTuple, Set<String> pnrs, String fromDate, String toDate) {
        this.carrierTuple = carrierTuple;
        this.pnrs = pnrs;
        this.fromDate = fromDate;
        this.toDate = toDate;
        pnrTnxIds = null;
    }

    public ReservationLot(CarrierTuple carrierTuple, Map<String, Set<String>> pnrTnxIds, String fromDate, String toDate) {
        this.carrierTuple = carrierTuple;
        this.pnrs = pnrTnxIds.keySet();
        this.pnrTnxIds = pnrTnxIds;
        this.fromDate = fromDate;
        this.toDate = toDate;
    }
}
