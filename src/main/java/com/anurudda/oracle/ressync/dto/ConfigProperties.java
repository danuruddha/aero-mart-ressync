package com.anurudda.oracle.ressync.dto;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix="app")
public class ConfigProperties {
    private boolean skipInserts;
    private String logPath;
    private boolean syncDummyBookings;
    private boolean syncExtTransactions;
    private boolean syncAgentAndPaxTransactions;
}