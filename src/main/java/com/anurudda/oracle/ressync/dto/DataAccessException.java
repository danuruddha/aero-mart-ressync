package com.anurudda.oracle.ressync.dto;

public class DataAccessException extends RuntimeException{
    public DataAccessException(){
        super();
    }
    public DataAccessException(String message){
        super(message);
    }
}
