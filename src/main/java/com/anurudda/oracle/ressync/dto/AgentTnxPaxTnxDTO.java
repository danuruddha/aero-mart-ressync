package com.anurudda.oracle.ressync.dto;

import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;

@Data
@ToString
public class AgentTnxPaxTnxDTO {
    private final String carrierCode;
    private String atPNR;
    private String ptPNR;
    private BigDecimal atAmount;
    private BigDecimal ptAmount;
    private BigDecimal difference;

    public AgentTnxPaxTnxDTO(String carrierCode){
        this.carrierCode = carrierCode;
    }
}
