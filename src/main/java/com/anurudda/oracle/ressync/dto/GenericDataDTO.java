package com.anurudda.oracle.ressync.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

@Data
public class GenericDataDTO {
    private String key;
    private Class type;
    private boolean currencyValue;
    private Object value;
    private boolean skipConverion;
    private boolean skipInsert;
    private boolean preserveOriginal;
    private boolean nullable;

    public GenericDataDTO(String key, String nullType, String type, String skipConversion, String skipInsert,
                          String preserveOriginal) {
        this.key = key;
        this.type = getCleanType(type);
        if(this.type == BigDecimal.class){
            currencyValue = true;
        }
        if(!"NOT NULL".equals(nullType)){
            this.nullable = true;
        }
        if("Y".equals(skipConversion)){
            this.skipConverion = true;
        }
        if("Y".equals(skipInsert)){
            this.skipInsert = true;
        }

        if("Y".equals(preserveOriginal)){
            this.preserveOriginal = true;
        }
    }

    private GenericDataDTO(){
    }

    public Object getValue(){
        if(this.isNullable() && this.value != null &&this.value.toString().trim().equals("")){
            return null;
        }
        return this.value;
    }

    public GenericDataDTO(String key, Class type, boolean currencyValue, Object value, boolean skipConverion,
                          boolean skipInsert, boolean preserveOriginal) {
        this.key = key;
        this.type = type;
        this.currencyValue = currencyValue;
        this.value = value;
        this.skipConverion = skipConverion;
        this.skipInsert = skipInsert;
        this.preserveOriginal = preserveOriginal;
    }

    public GenericDataDTO clone() {
        GenericDataDTO newObj = new GenericDataDTO();
        newObj.setKey(this.key);
        newObj.setType(this.type);
        newObj.setCurrencyValue(this.currencyValue);
        newObj.setValue(this.value);
        newObj.setSkipConverion(this.skipConverion);
        newObj.setSkipInsert(this.skipInsert);
        newObj.setPreserveOriginal(this.preserveOriginal);
        newObj.setNullable(this.nullable);
        return newObj;
    }

    public boolean needToConvert(){
        return isCurrencyValue() && !isSkipConverion();
    }

    private Class getCleanType(String type) {
        if(type.contains("VARCHAR") || type.contains("CHAR")){
            return String.class;
        }else if (type.contains("NUMBER") && type.contains(",")){
            return BigDecimal.class;
        } else if (type.contains("NUMBER")){
            return Long.class;
        } else if (type.contains("DATE")){
            return Date.class;
        } else {
            throw new RuntimeException("Invalid data type");
        }
    }

    public Object getData(ResultSet rs) throws SQLException {
        if(this.type.equals(String.class)){
           return rs.getString(this.key);
        } else if (this.type.equals(BigDecimal.class)){
            return rs.getBigDecimal(this.key);
        } else if (this.type.equals(Long.class)){
            return rs.getLong(this.key);
        } else if (this.type.equals(Date.class)){
            return rs.getDate(this.key);
        }
        return null;
    }

}
