package com.anurudda.oracle.ressync.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class PaxExtTransactionDTO extends BaseDTO{
    private String carrierCode;
    public PaxExtTransactionDTO(String carrierCode){
        this.carrierCode = carrierCode;
    }



    public String getPNR(){
        return (String) getValue("PNR");
    }

    public Long getPNRPaxId() {
        return (Long) getValue("PNR_PAX_ID");
    }

    public Long getPaxSeqNo() {
        return (Long) getValue("PAX_SEQUENCE");
    }
    public String getLCCTnxId() {
        return (String) getValue("LCC_UNIQUE_TXN_ID");
    }

    public String getUniquePaxSeqNo() {
        return getPNR()+"|"+getPaxSeqNo();
    }
}
