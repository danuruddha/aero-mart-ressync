package com.anurudda.oracle.ressync.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

@Data
@EqualsAndHashCode(callSuper=false)
public class AgentTransactionDTO extends BaseDTO{
    private final String carrierCode;

    public AgentTransactionDTO(String carrierCode) {
        super();
        this.carrierCode = carrierCode;
    }

    public String getAgentCode(){
        return (String) getValue("AGENT_CODE");
    }
    public String getPNR(){
        return (String) getValue("PNR");
    }

    public BigDecimal getAmount(){
        return (BigDecimal) getValue("AMOUNT");
    }
    public BigDecimal getAmountLocal(){
        return (BigDecimal) getValue("AMOUNT_LOCAL");
    }

    public void setAmount(BigDecimal amount){
         setValue("AMOUNT", amount);
    }
    public void setAmountLocal(BigDecimal amountLocal){
        setValue("AMOUNT_LOCAL", amountLocal);
    }

    public AgentTransactionDTO clone(){
        AgentTransactionDTO dto= new AgentTransactionDTO(this.carrierCode);
        dto.setNeedSync(this.isNeedSync());
        for(String key: dataMap.keySet()){
            dto.addData(key, dataMap.get(key).clone());
        }
        return dto;
    }

    public void setNominalCodesFor(String crdr) {
        if("DR".equalsIgnoreCase(crdr)){
            GenericDataDTO data = getDataMap().get("NOMINAL_CODE");
            data.setValue(1L);
            getDataMap().get("DR_CR").setValue("DR");
        }else if ("CR".equalsIgnoreCase(crdr)){
            GenericDataDTO data = getDataMap().get("NOMINAL_CODE");
            data.setValue(7L);
            getDataMap().get("DR_CR").setValue("CR");
        }
    }
}
