package com.anurudda.oracle.ressync.dto;

public class CarrierTuple {
    public final String mktCarrier;
    public final String opCarrier;


    public CarrierTuple(String mktCarrier, String opCarrier) {
        this.mktCarrier = mktCarrier;
        this.opCarrier = opCarrier;
    }
}
