package com.anurudda.oracle.ressync.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper=false)
public class BasicReservationDTO extends BaseDTO{

    public BasicReservationDTO(){

    }

    private CarrierTuple carrierTuple = null;
    private String carrierCode;

    public BasicReservationDTO(String carrierCode) {
        super();
        this.carrierCode = carrierCode;
    }

    public String getPNR(){
        return (String) getValue("PNR");
    }

    public Date getBookingDate(){
        return (Date) getValue("BOOKING_TIMESTAMP");
    }

}
