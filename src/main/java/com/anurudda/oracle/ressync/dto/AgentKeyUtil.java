package com.anurudda.oracle.ressync.dto;

import java.text.SimpleDateFormat;
import java.util.*;

public class AgentKeyUtil {
    public static final int ACCEPTABLE_TNX_TIME_DIFFERENCE_IN_MILLIS = 30000;
    private List<Date> allDates = new ArrayList<>();
    private Map<Date, Date> dateMap = new HashMap<>();
    public String getAgentTnxKey(Map<String, GenericDataDTO> dataMap) {
        StringBuilder sb = new StringBuilder();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-dd-MM HH:mm");
        sb.append(sdf.format( getTnxDate((Date)dataMap.get("TNX_DATE").getValue())));
        sb.append("|");
        sb.append(dataMap.get("AGENT_CODE").getValue().toString());
        sb.append("|");
        sb.append(dataMap.get("NOMINAL_CODE").getValue().toString());
        return sb.toString();
    }
    private Date getTnxDate(Date tnxDate){
        if(!dateMap.containsKey(tnxDate)) {
            Date closeDate = null;
            Long currentMinDiff = null;
            for (Date date : allDates) {
                long difference = date.getTime() - tnxDate.getTime();
                if (difference < ACCEPTABLE_TNX_TIME_DIFFERENCE_IN_MILLIS && (currentMinDiff == null || currentMinDiff > difference)) {
                    closeDate = date;
                    currentMinDiff = difference;
                    break;
                }
            }
            if (closeDate == null) {
                closeDate = tnxDate;
            }
            allDates.add(tnxDate);
            if (!dateMap.containsKey(tnxDate)) {
                dateMap.put(tnxDate, closeDate);
            }
        }
        return dateMap.get(tnxDate);
    }
}
