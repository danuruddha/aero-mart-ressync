package com.anurudda.oracle.ressync.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
public abstract class BaseDTO {
    protected Map<String, GenericDataDTO> dataMap = new HashMap<>();
    private boolean needSync = false;
    public void addData(String key, GenericDataDTO data){
        dataMap.put(key, data);
    }

    public Object getValue(String key){
        if(getDataMap().containsKey(key)){
            return getDataMap().get(key).getValue();
        }
        return null;
    }
    public void setValue(String key, Object object){
        getDataMap().get(key).setValue(object);
    }
}
