package com.anurudda.oracle.ressync.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class ExchangeRateDTO {
    private String airlineA = null;
    private String airlineB = null;
    private BigDecimal fromAtoB = null;
    private BigDecimal fromBtoA = null;
    private Date effectiveFrom = null;
    private Date effectiveTo = null;

    public boolean isWithinRange(Date date){
        if(date.compareTo(effectiveFrom)>=0 && date.compareTo(effectiveTo)<=0){
            return true;
        }
        return false;
    }
    public boolean isAtoB(String from, String to){
        return airlineA.equalsIgnoreCase(from)&&airlineB.equalsIgnoreCase(to);
    }
    public boolean isBtoA(String from, String to){
        return airlineB.equalsIgnoreCase(from)&&airlineA.equalsIgnoreCase(to);
    }
}
