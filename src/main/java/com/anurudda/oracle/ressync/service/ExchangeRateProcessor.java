package com.anurudda.oracle.ressync.service;

import com.anurudda.oracle.ressync.dto.ExchangeRateDTO;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;

public class ExchangeRateProcessor {
    private final List<ExchangeRateDTO> excRates;
    private int precision = 2;
    public ExchangeRateProcessor(List<ExchangeRateDTO> excRates) {
        this.excRates = excRates;
    }
    public ExchangeRateProcessor(List<ExchangeRateDTO> excRates, int precision) {
        this(excRates);
        this.precision = precision;
    }
    public BigDecimal convert(Date date, BigDecimal amount, String fromCarrier, String toCarrier) {
        if(fromCarrier.equalsIgnoreCase(toCarrier)){
            return amount;
        }
        BigDecimal exRate = getExchangeRate(date, fromCarrier, toCarrier);

        BigDecimal newAmount = exRate.multiply(amount);
        newAmount = newAmount.setScale(precision, RoundingMode.HALF_UP);
        return newAmount;
    }

    private BigDecimal getExchangeRate(Date date, String fromCarrier, String toCarrier) {
        BigDecimal exRate = null;
        for(ExchangeRateDTO x: excRates){
            if(x.isWithinRange(date) && x.isAtoB(fromCarrier,toCarrier)){
                exRate= x.getFromAtoB();
                break;
            }
        }
        if(exRate == null) {
            for (ExchangeRateDTO x : excRates) {
                if (x.isWithinRange(date) && x.isBtoA(fromCarrier, toCarrier)) {
                    exRate = x.getFromBtoA();
                    break;
                }
            }
        }
        return exRate;
    }
}
