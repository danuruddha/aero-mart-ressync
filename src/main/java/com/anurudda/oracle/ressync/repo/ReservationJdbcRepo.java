package com.anurudda.oracle.ressync.repo;

import com.anurudda.oracle.ressync.dto.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class ReservationJdbcRepo {

    private final Logger logger = LoggerFactory.getLogger(getClass().getName());

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;
    @Autowired
    private AuditRepo auditRepo;

    @Autowired
    private TableConfig config;

    @Autowired
    private ConfigProperties properties;

    public Set<String> getDummyCarrierMissingPnrs(String fromDateString, String toDateString, CarrierTuple tuple) {
        Set<String> pnrs = new HashSet<>();
        MapSqlParameterSource paramSource = new MapSqlParameterSource();
        paramSource.addValue("fromDate", fromDateString);
        paramSource.addValue("toDate", toDateString);
        paramSource.addValue("mktCarrierCode", tuple.mktCarrier);

        String mktSchema = getSchema(tuple.mktCarrier);
        String opSchema = getSchema(tuple.opCarrier);
        String sql = "select pnr from " + opSchema + "t_pnr_passenger p," + opSchema + "t_pax_transaction pt " +
                "where p.pnr_pax_id=pt.pnr_pax_id and payment_carrier_code=:mktCarrierCode and " +
                "TNX_DATE  BETWEEN  to_date(:fromDate,'dd-MON-yyyy HH24:MI:ss') " +
                "and to_date(:toDate,'dd-MON-yyyy HH24:MI:ss') and amount<>0 " +
                "and  nominal_code in ('19','25','36','37') " +
                " MINUS " +
                "select pnr from " + mktSchema + "t_reservation ";
        logQuery(sql, paramSource);
        jdbcTemplate.query(sql, paramSource, rs -> {
            pnrs.add(rs.getString("pnr"));
        });
        return pnrs;
    }


    public Map<String, BasicReservationDTO> getBasicReservationDetailsForPnrs(Set<String> pnrs, CarrierTuple tuple) {
        MapSqlParameterSource paramSource = new MapSqlParameterSource();
        paramSource.addValue("pnrs", pnrs);
        String sql = "select * from " + getSchema(tuple.opCarrier) + "t_reservation where pnr in (:pnrs) ";
        logQuery(sql, paramSource);
        final Map<String, BasicReservationDTO> resultMap = new HashMap<>();
        jdbcTemplate.query(sql, paramSource, (rs, rowNum) -> {
                    BasicReservationDTO res = new BasicReservationDTO(tuple.opCarrier);
                    res.setCarrierTuple(tuple);
                    Set<GenericDataDTO> columnConfigs = config.getDataListForTable("t_reservation");
                    for (GenericDataDTO data : columnConfigs) {
                        Object value = data.getData(rs);
                        GenericDataDTO resultObj = data.clone();
                        resultObj.setValue(value);
                        res.addData(data.getKey(), resultObj);
                    }
                    resultMap.put(res.getPNR(), res);
                    return res;
                }
        );
        return resultMap;
    }

    public Map<String, List<PassengerDTO>> getPassengerDetailsForPnrs(Set<String> pnrs, CarrierTuple tuple) {
        MapSqlParameterSource paramSource = new MapSqlParameterSource();
        paramSource.addValue("pnrs", pnrs);
        String sql = "select * from " + getSchema(tuple.opCarrier) + "t_pnr_passenger where pnr in (:pnrs) ";
        logQuery(sql, paramSource);
        final Map<String, List<PassengerDTO>> resultMap = new HashMap<>();

        jdbcTemplate.query(sql, paramSource, (rs, rowNum) -> {
            PassengerDTO pax = new PassengerDTO(tuple.opCarrier);
            Set<GenericDataDTO> columnConfigs = config.getDataListForTable("t_pnr_passenger");
            for (GenericDataDTO data : columnConfigs) {
                Object value = data.getData(rs);
                GenericDataDTO resultObj = data.clone();
                resultObj.setValue(value);
                pax.addData(data.getKey(), resultObj);
            }
            if (!resultMap.containsKey(pax.getPNR())) {
                resultMap.put(pax.getPNR(), new ArrayList<>());
            }
            resultMap.get(pax.getPNR()).add(pax);
            return pax;
        });
        return resultMap;
    }

    public Map<String, List<PaxTransactionDTO>> getPaxTransactionsForPnrs(Set<String> pnrs, CarrierTuple tuple, String fromDate, String toDate) {
        MapSqlParameterSource paramSource = new MapSqlParameterSource();
        paramSource.addValue("pnrs", pnrs);
        paramSource.addValue("mktCarrierCode", tuple.mktCarrier);
        if (fromDate != null && toDate != null) {
            paramSource.addValue("fromDate", fromDate);
            paramSource.addValue("toDate", toDate);
        }
        String opSchema = getSchema(tuple.opCarrier);

        String sql = "select p.pnr,p.pax_sequence, pt.* from " + opSchema + "t_pnr_passenger p, " + opSchema + "t_pax_transaction pt " +
                "where p.pnr_pax_id=pt.pnr_pax_id " +
                "and payment_carrier_code=:mktCarrierCode " +
                "and p.pnr in (:pnrs) and amount<>0 " +
                "and nominal_code in ('19','25') ";
        //,'36','37') "; //+
        if (fromDate != null && toDate != null) {
            sql = sql + " and pt.TNX_DATE  BETWEEN  to_date(:fromDate,'dd-MON-yyyy HH24:MI:ss') " +
                    " and to_date(:toDate,'dd-MON-yyyy HH24:MI:ss') ";
        }
        logQuery(sql, paramSource);
        final Map<String, List<PaxTransactionDTO>> resultMap = new HashMap<>();

        jdbcTemplate.query(sql, paramSource, new RowMapper<PaxTransactionDTO>() {
            @Override
            public PaxTransactionDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
                PaxTransactionDTO paxTnx = new PaxTransactionDTO(tuple.opCarrier);

                Set<GenericDataDTO> columnConfigs = config.getDataListForTable("t_pax_transaction");
                for (GenericDataDTO data : columnConfigs) {
                    GenericDataDTO resultObj = data.clone();
                    resultObj.setValue(data.getData(rs));
                    paxTnx.addData(data.getKey(), resultObj);
                }
                if (!resultMap.containsKey(paxTnx.getUniquePaxSeqNo())) {
                    resultMap.put(paxTnx.getUniquePaxSeqNo(), new ArrayList<>());
                }
                resultMap.get(paxTnx.getUniquePaxSeqNo()).add(paxTnx);
                return paxTnx;

            }
        });
        return resultMap;
    }

    public Map<String, List<PaxExtTransactionDTO>> getPaxExtTransactionsForPnrs(Set<String> pnrs, CarrierTuple tuple, String fromDate, String toDate) {
        MapSqlParameterSource paramSource = new MapSqlParameterSource();
        paramSource.addValue("pnrs", pnrs);
        if (tuple.opCarrier != null)
            paramSource.addValue("opCarrierCode", tuple.opCarrier);
        if (fromDate != null && toDate != null) {
            paramSource.addValue("fromDate", fromDate);
            paramSource.addValue("toDate", toDate);
        }

        String mktSchema = getSchema(tuple.mktCarrier);
        String sql = "select p.pnr,p.pax_sequence, pect.* FROM " + mktSchema + "t_pnr_passenger p," +
                mktSchema + "t_pax_ext_carrier_transactions pect " +
                "WHERE pect.pnr_pax_id=p.pnr_pax_id " +
                " and p.pnr in (:pnrs)" +
                " and pect.nominal_code in (19,25) ";
        if (tuple.opCarrier != null) {
            sql = sql + " and ext_carrier_code = :opCarrierCode ";
        }


        if (fromDate != null && toDate != null) {
            sql = sql + " and pect.TXN_TIMESTAMP between  to_date(:fromDate,'dd-MON-yyyy HH24:MI:ss') " +
                    " and to_date(:toDate,'dd-MON-yyyy HH24:MI:ss') ";
        }
        logQuery(sql, paramSource);
        final Map<String, List<PaxExtTransactionDTO>> resultMap = new HashMap<>();

        jdbcTemplate.query(sql, paramSource, new RowMapper<PaxExtTransactionDTO>() {
            @Override
            public PaxExtTransactionDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
                PaxExtTransactionDTO paxTnx = new PaxExtTransactionDTO(tuple.mktCarrier);

                Set<GenericDataDTO> columnConfigs = config.getDataListForTable("t_pax_ext_carrier_transactions");
                for (GenericDataDTO data : columnConfigs) {
                    GenericDataDTO resultObj = data.clone();
                    resultObj.setValue(data.getData(rs));
                    paxTnx.addData(data.getKey(), resultObj);
                }
                if (!resultMap.containsKey(paxTnx.getUniquePaxSeqNo())) {
                    resultMap.put(paxTnx.getUniquePaxSeqNo(), new ArrayList<>());
                }
                resultMap.get(paxTnx.getUniquePaxSeqNo()).add(paxTnx);
                return paxTnx;

            }
        });
        return resultMap;
    }


    public List<ExchangeRateDTO> getLCCExchangeRatesFor(final String airlineA, final String airlineB,
                                                        String fromDate, String toDate) {
        MapSqlParameterSource paramSource = new MapSqlParameterSource();
        paramSource.addValue("fromDate", fromDate);
        paramSource.addValue("toDate", toDate);

        paramSource.addValue("airlineA", airlineA);
        paramSource.addValue("airlineB", airlineB);

        String lccSchema = getSchema("LCC");

        String sql = "select * from " + lccSchema + "lcc_t_int_airline_ex_rate where airline_code_1 = :airlineA " +
                "and airline_code_2 = :airlineB and to_date(:toDate,'dd-MON-yyyy HH24:MI:ss') >= effective_from " +
                " and effective_to >= to_date(:fromDate,'dd-MON-yyyy HH24:MI:ss') " +
                " and status = 'ACT' ";
        logQuery(sql, paramSource);
        return jdbcTemplate.query(sql, paramSource, new RowMapper<ExchangeRateDTO>() {
            @Override
            public ExchangeRateDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
                ExchangeRateDTO exRate = new ExchangeRateDTO();
                exRate.setAirlineA(airlineA);
                exRate.setAirlineB(airlineB);
                exRate.setFromAtoB(rs.getBigDecimal("ac1_to_ac2_exchange_rate"));
                exRate.setFromBtoA(rs.getBigDecimal("ac2_to_ac1_exchange_rate"));
                exRate.setEffectiveFrom(rs.getDate("effective_from"));
                exRate.setEffectiveTo(rs.getDate("effective_to"));
                return exRate;
            }
        });
    }

    private void logQuery(String sql, MapSqlParameterSource source) {
        List<String> sortedList = source.getValues().keySet().stream().sorted((a1, a2) -> a2.length() - a1.length())
                .collect(Collectors.toList());
        ;
        for (String key : sortedList) {
            //            logger.debug("key :{}, value:{}", key, getValue(source, key));
            sql = sql.replaceAll(":" + key, getValue(source, key));
        }
        auditRepo.auditChanges(sql);
        logger.debug("\nQUERY: \n{}", sql);
    }

    private String getValue(MapSqlParameterSource source, String key) {
        if (source.getValue(key) != null) {
            if (source.getValue(key) instanceof Collection) {
                Collection list = (Collection) source.getValue(key);
                return "'" + list.stream().map(Object::toString).collect(Collectors.joining("','")) + "'";
            } else if (source.getValue(key) instanceof BigDecimal) {
                return ((BigDecimal) source.getValue(key)).toPlainString();
            } else if (source.getValue(key) instanceof Long) {
                return ((Long) source.getValue(key)).toString();
            } else if (source.getValue(key) instanceof Date) {
                return "to_date('" + CommonUtil.formatDate((Date) source.getValue(key)) + "','" + CommonUtil.DATE_FMT_ORACLE + "')";
            } else {
                if(source.getValue(key).toString().equals("NULL")){
                    return source.getValue(key).toString();
                }
                return "'" + source.getValue(key).toString() + "'";
            }
        }
        return "";
    }

    private String getSchema(String carrierCode) {
        String schema = "";
        switch (carrierCode) {
            case "G9":
                schema = "aa_mgrlive.";
                break;
            case "3O":
                schema = "ma_mgrlive.";
                break;
            case "3L":
                schema = "ad_mgrlive.";
                break;
            case "E5":
                schema = "eg_mgrlive.";
                break;
            case "LCC":
                schema = "lcc2_mgrlive.";
                break;
        }
        return schema;
    }


    public void createMissingDummyReservation(BasicReservationDTO basic) {

        MapSqlParameterSource paramSource = new MapSqlParameterSource();
        MapSqlParameterSource paramSourceLogging = new MapSqlParameterSource();

        String schema = getSchema(basic.getCarrierTuple().mktCarrier);
        StringBuilder columns = new StringBuilder();
        StringBuilder params = new StringBuilder();
        for (String key : basic.getDataMap().keySet()) {
            GenericDataDTO data = basic.getDataMap().get(key);
            if (data.isSkipInsert()) {
                continue;
            }
            if (columns.toString().length() > 0) {
                columns.append(",");
                params.append(",");
            }
            columns.append(key);
            params.append(":" + key);
            paramSource.addValue(key, data.getValue());
            paramSourceLogging.addValue(key, getNormalizedValueForLogging(data));
        }
        String sql = "insert into " + schema + "t_reservation (" + columns.toString() + ") values (" +
                params.toString() + ") ";
        logQuery(sql, paramSourceLogging);
        if (!properties.isSkipInserts()) {
            int response = jdbcTemplate.update(sql, paramSource);
            if (response == 0) {
                throw new DataAccessException("Update failed");
            }
        }
    }

    private Object getNormalizedValueForLogging(GenericDataDTO data) {
        return data.getValue()==null?"NULL": data.getValue();
    }

    public void createMissingDummyPassenger(PassengerDTO pax, CarrierTuple carrierTuple) {
        MapSqlParameterSource paramSource = new MapSqlParameterSource();
        MapSqlParameterSource paramSourceLogging = new MapSqlParameterSource();
        String schema = getSchema(carrierTuple.mktCarrier);
        StringBuilder columns = new StringBuilder();
        StringBuilder params = new StringBuilder();
        for (String key : pax.getDataMap().keySet()) {
            GenericDataDTO data = pax.getDataMap().get(key);
            if (data.isSkipInsert()) {
                continue;
            }
            if (columns.toString().length() > 0) {
                columns.append(",");
                params.append(",");
            }
            columns.append(key);
            params.append(":" + key);
            paramSource.addValue(key, data.getValue());
            paramSourceLogging.addValue(key, getNormalizedValueForLogging(data));
        }
        String sql = "insert into " + schema + "t_pnr_passenger (PNR_PAX_ID," + columns.toString() + ") values (" +
                "S_PNR_PASSENGER.nextval," +
                params.toString() + ") ";
        logQuery(sql, paramSourceLogging);
        if (!properties.isSkipInserts()) {
            int response = jdbcTemplate.update(sql, paramSource);
            if (response == 0) {
                throw new DataAccessException("Update failed");
            }
        }
    }

    public PassengerDTO getPassenger(String pnr, Long paxSequence, CarrierTuple carrierTuple) {
        MapSqlParameterSource paramSource = new MapSqlParameterSource();
        paramSource.addValue("pnr", pnr);
        paramSource.addValue("paxSequence", paxSequence);
        String sql = "select * from " + getSchema(carrierTuple.mktCarrier) + "t_pnr_passenger where pnr = :pnr" +
                " and pax_sequence = :paxSequence ";
        logQuery(sql, paramSource);

        return jdbcTemplate.queryForObject(sql, paramSource, new RowMapper<PassengerDTO>() {
            @Override
            public PassengerDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
                PassengerDTO pax = new PassengerDTO(carrierTuple.opCarrier);
                Set<GenericDataDTO> columnConfigs = config.getDataListForTable("t_pnr_passenger");
                for (GenericDataDTO data : columnConfigs) {
                    Object value = data.getData(rs);
                    GenericDataDTO resultObj = data.clone();
                    resultObj.setValue(value);
                    pax.addData(data.getKey(), resultObj);
                }
                return pax;
            }
        });
    }

    public void addMissingPaxExtTransactions(PaxTransactionDTO paxTransaction, Map<Long, Long> opMktPaxIdMap,
                                             CarrierTuple carrierTuple, String pnr, Long paxSequence) {
        MapSqlParameterSource paramSource = new MapSqlParameterSource();
        MapSqlParameterSource paramSourceLogging = new MapSqlParameterSource();
        String schema = getSchema(carrierTuple.mktCarrier);
        StringBuilder columns = new StringBuilder();
        StringBuilder params = new StringBuilder();

        Map<String, String> mapping = config.getTableToTableMapping("t_pax_ext_carrier_tnx_to_t_pax_tnx");
        for (String opKey : mapping.keySet()) {
            String mktKey = mapping.get(opKey);
//            logger.debug("opKey={} mktKey={}", opKey, mktKey);
            GenericDataDTO data = paxTransaction.getDataMap().get(mktKey);
            if (data.isSkipInsert()) {
                continue;
            }
            if (columns.toString().length() > 0) {
                columns.append(",");
                params.append(",");
            }
            columns.append(opKey);
            params.append(":" + mktKey);
            paramSource.addValue(mktKey, data.getValue());
            paramSourceLogging.addValue(mktKey, getNormalizedValueForLogging(data));

        }

        if (columns.toString().length() > 0) {
            columns.append(",");
            params.append(",");
        }

        Long pnrPaxId = opMktPaxIdMap.get(
                paxTransaction.getDataMap().get("PNR_PAX_ID").getValue());

        if(pnrPaxId != null) {
            columns.append("PNR_PAX_ID");
            params.append(":PNR_PAX_ID");
            paramSource.addValue("PNR_PAX_ID", pnrPaxId);
            paramSourceLogging.addValue("PNR_PAX_ID", pnrPaxId);
        }
        if (columns.toString().length() > 0) {
            columns.append(",");
            params.append(",");
        }
        columns.append("EXT_CARRIER_CODE");
        params.append(":EXT_CARRIER_CODE");
        paramSource.addValue("EXT_CARRIER_CODE", carrierTuple.opCarrier);
        paramSourceLogging.addValue("EXT_CARRIER_CODE", carrierTuple.opCarrier);


        String sql = "insert into " + schema + "t_pax_ext_carrier_transactions (PAX_EXT_CARRIER_TXN_ID," + columns.toString() + ") values (" +
                "S_PAX_TRANSACTION.nextval," +
                params.toString() + ") ";

        if(pnrPaxId == null){
            paramSourceLogging.addValue("pnr", pnr);
            paramSourceLogging.addValue("paxSequence", paxSequence);
            sql = "insert into " + schema + "t_pax_ext_carrier_transactions (PAX_EXT_CARRIER_TXN_ID,PNR_PAX_ID,"
                    + columns.toString() + ") values (" +
                    "S_PAX_TRANSACTION.nextval,(select PNR_PAX_ID from " + schema + "t_pnr_passenger where pnr=:pnr " +
                    "and pax_sequence=:paxSequence)," +
                    params.toString() + ")  ";
        }

        logQuery(sql, paramSourceLogging);
        if (!properties.isSkipInserts()) {
            int response = jdbcTemplate.update(sql, paramSource);
            if(response == 0){
                throw new DataAccessException("Update failed");
            }
        }
    }

    public void addMissingAgentTransactions(AgentTransactionDTO agentTnx, CarrierTuple carrierTuple) {
        MapSqlParameterSource paramSource = new MapSqlParameterSource();
        MapSqlParameterSource paramSourceLogging = new MapSqlParameterSource();
        String schema = getSchema(carrierTuple.mktCarrier);
        StringBuilder columns = new StringBuilder();
        StringBuilder params = new StringBuilder();

        for (String key : agentTnx.getDataMap().keySet()) {
            GenericDataDTO data = agentTnx.getDataMap().get(key);
            if (data.isSkipInsert()) {
                continue;
            }
            if (columns.toString().length() > 0) {
                columns.append(",");
                params.append(",");
            }
            columns.append(key);
            params.append(":" + key);
            paramSource.addValue(key, data.getValue());
            paramSourceLogging.addValue(key, getNormalizedValueForLogging(data));

        }


        String sql = "insert into " + schema + "t_agent_transaction (TXN_ID," + columns.toString() + ") values (" +
                "S_AGENT_TRANSACTION.nextval," +
                params.toString() + ") ";
        logQuery(sql, paramSourceLogging);
        if (!properties.isSkipInserts()) {
            int response = jdbcTemplate.update(sql, paramSource);
            if (response == 0) {
                throw new DataAccessException("Update failed");
            }
        }
    }

    public Map<String, List<AgentTransactionDTO>> getAgentTransactionForPnrs(Set<String> pnrs, CarrierTuple tuple, String fromDate, String toDate) {
        MapSqlParameterSource paramSource = new MapSqlParameterSource();
        paramSource.addValue("pnrs", pnrs);
        if (fromDate != null && toDate != null) {
            paramSource.addValue("fromDate", fromDate);
            paramSource.addValue("toDate", toDate);
        }
        String mktSchema = getSchema(tuple.mktCarrier);
        String sql = "select * from " + mktSchema + "t_agent_transaction where pnr in (:pnrs) ";
        if (fromDate != null && toDate != null) {
            sql = sql + " and tnx_date between  to_date(:fromDate,'dd-MON-yyyy HH24:MI:ss') " +
                    " and to_date(:toDate,'dd-MON-yyyy HH24:MI:ss') ";
        }
        logQuery(sql, paramSource);
        final Map<String, List<AgentTransactionDTO>> resultMap = new HashMap<>();

        jdbcTemplate.query(sql, paramSource, new RowMapper<AgentTransactionDTO>() {
            @Override
            public AgentTransactionDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
                AgentTransactionDTO agentTnx = new AgentTransactionDTO(tuple.mktCarrier);

                Set<GenericDataDTO> columnConfigs = config.getDataListForTable("t_agent_transaction");
                for (GenericDataDTO data : columnConfigs) {
                    GenericDataDTO resultObj = data.clone();
                    resultObj.setValue(data.getData(rs));
                    agentTnx.addData(data.getKey(), resultObj);
                }
                if (!resultMap.containsKey(agentTnx.getPNR())) {
                    resultMap.put(agentTnx.getPNR(), new ArrayList<>());
                }
                resultMap.get(agentTnx.getPNR()).add(agentTnx);
                return agentTnx;

            }
        });
        return resultMap;
    }

    public void adjustAgentTransactionSummary(AgentTransactionDTO agentTnx, CarrierTuple carrierTuple) {
        MapSqlParameterSource paramSource = new MapSqlParameterSource();
        String schema = getSchema(carrierTuple.mktCarrier);
        paramSource.addValue("agentCode", agentTnx.getAgentCode());
        paramSource.addValue("amount", agentTnx.getAmount());

        String sql = "update " + schema + "t_agent_summary " +
                "set avilable_credit = avilable_credit - (:amount), SHARED_CREDIT_DUE_AMOUNT = SHARED_CREDIT_DUE_AMOUNT + (:amount) " +
                "where agent_code = :agentCode ";
        logQuery(sql, paramSource);

        if (!properties.isSkipInserts()) {
            int response = jdbcTemplate.update(sql, paramSource);
            if(response == 0){
                throw new DataAccessException("Update failed");
            }
        }
    }

    public Map<String, Set<String>> getMissingExtTransactions(String fromDate, String toDate, CarrierTuple tuple) {
        MapSqlParameterSource paramSource = new MapSqlParameterSource();
        String mktSchema = getSchema(tuple.mktCarrier);
        String opSchema = getSchema(tuple.opCarrier);

        paramSource.addValue("fromDate", fromDate);
        paramSource.addValue("toDate", toDate);
        paramSource.addValue("paymentCarrier", tuple.mktCarrier);

        String sql = "select pnr,pt.lcc_unique_txn_id from " + opSchema + "t_pnr_passenger p," +
                opSchema + "t_pax_transaction pt " +
                " where p.pnr_pax_id=pt.pnr_pax_id and payment_carrier_code=:paymentCarrier and tnx_date  " +
                " between  to_date(:fromDate,'dd-MON-yyyy HH24:MI:ss') " +
                " and to_date(:toDate,'dd-MON-yyyy HH24:MI:ss') and amount<>0  " +
                " and  nominal_code in (19,25)  " +
                " and lcc_unique_txn_id is not null " +
                " minus " +
                " select pnr,pect.lcc_unique_txn_id FROM " + mktSchema + "t_pnr_passenger P," +
                mktSchema + "t_pax_ext_carrier_transactions pect " +
                " WHERE pect.pnr_pax_id=P.pnr_pax_id" +
                " and pect.nominal_code in (19,25) " +
                " and pect.txn_timestamp " +
                " between  to_date(:fromDate,'dd-MON-yyyy HH24:MI:ss') " +
                " and to_date(:toDate,'dd-MON-yyyy HH24:MI:ss') ";
        logQuery(sql, paramSource);
        Map<String, Set<String>> pnrTnxIdMap = new HashMap<>();
        jdbcTemplate.query(sql, paramSource, rs -> {
            String pnr = rs.getString("pnr");
            if (!pnrTnxIdMap.containsKey(pnr)) {
                pnrTnxIdMap.put(pnr, new HashSet<>());
            }
            pnrTnxIdMap.get(pnr).add(rs.getString("lcc_unique_txn_id"));
        });
        return pnrTnxIdMap;
    }

    public List<String> getAgentPaxTransactionMismatchingAgents(String fromDate, String toDate, CarrierTuple tuple) {
        MapSqlParameterSource paramSource = new MapSqlParameterSource();
        String opSchema = getSchema(tuple.opCarrier);

        paramSource.addValue("fromDate", fromDate);
        paramSource.addValue("toDate", toDate);

        String sql = "SELECT AT.AGENT_CODE AT_AGENT_CODE, AT.AMT AT_AMT, PT.AGENT_CODE PT_AGENT_CODE, PT.AMT PT_AMT, " +
                " nvl(at.amt,0)+nvl(pt.amt,0) DIFF FROM  " +
                "(SELECT AGENT_CODE, SUM(AGT.amount) as AMT FROM " + opSchema + "T_AGENT_TRANSACTION AGT " +
                "WHERE AGT.tnx_date BETWEEN to_date(:fromDate,'dd-MON-yyyy HH24:MI:ss')  " +
                "and to_date(:toDate,'dd-MON-yyyy HH24:MI:ss') " +
                "AND AGT.nominal_code IN (1,7) " +
                "AND AGT.agent_code IN (SELECT AGENT_CODE FROM " + opSchema + "T_AGENT AG " +
                " WHERE (AG.is_lcc_integration_agent = 'N' OR AG.is_lcc_integration_agent IS NULL)  " +
                " AND AG.airline_code = (select PARAM_VALUE from " + opSchema + "t_app_parameter where param_KEY='AIRLINE_71')) " +
                " group by AGT.AGENT_CODE) AT, " +
                "(SELECT  AGENT_CODE,SUM(AMT) AS AMT from " +
                "    (SELECT A.AGENT_CODE, A.AMOUNT AMT FROM " + opSchema + "T_PAX_TRANSACTION A, " +
                opSchema + "T_AGENT G, " + opSchema + "T_PNR_PASSENGER P " +
                "      WHERE A.NOMINAL_CODE IN ( '19','25' ) " +
                "    AND A.TNX_DATE BETWEEN to_date(:fromDate,'dd-MON-yyyy HH24:MI:ss') " +
                " and to_date(:toDate,'dd-MON-yyyy HH24:MI:ss')  " +
                "    AND A.AGENT_CODE                = G.AGENT_CODE(+) " +
                "    AND (A.PAYMENT_CARRIER_CODE     = (select PARAM_VALUE from " + opSchema + "t_app_parameter where" +
                " param_KEY='AIRLINE_13') OR A.PAYMENT_CARRIER_CODE      IS NULL ) " +
                "    AND (G.IS_LCC_INTEGRATION_AGENT = 'N' OR G.IS_LCC_INTEGRATION_AGENT  IS NULL)  " +
                "    AND P.PNR_PAX_ID = A.PNR_PAX_ID " +
                "      UNION ALL      " +
                "     SELECT A.AGENT_CODE, A.AMOUNT AMT FROM " + opSchema + "T_PAX_EXT_CARRIER_TRANSACTIONS A, " +
                opSchema + "T_PNR_PASSENGER P " +
                "      WHERE A.NOMINAL_CODE IN ('19','25') " +
                "    AND A.TXN_TIMESTAMP BETWEEN to_date(:fromDate,'dd-MON-yyyy HH24:MI:ss') " +
                " and to_date(:toDate,'dd-MON-yyyy HH24:MI:ss') " +
                "    AND A.PNR_PAX_ID = P.PNR_PAX_ID " +
                "    ) " +
                "    group by AGENT_CODE ) PT " +
                "where PT.AGENT_CODE = AT.AGENT_CODE(+) and (nvl(at.amt,0)+nvl(pt.amt,0))<> 0 ";
        logQuery(sql, paramSource);
        return jdbcTemplate.query(sql, paramSource, (rs, rowNum) -> {
            if (rs.getString("AT_AGENT_CODE") != null) {
                return rs.getString("AT_AGENT_CODE");
            }
            return rs.getString("PT_AGENT_CODE");

        });
    }

    public List<AgentTnxPaxTnxDTO> getAgentPaxTransactionMismatches(String fromDate, String toDate, CarrierTuple tuple,
                                                                    List<String> mismatchingAgents) {
        MapSqlParameterSource paramSource = new MapSqlParameterSource();
        String opSchema = getSchema(tuple.opCarrier);

        paramSource.addValue("fromDate", fromDate);
        paramSource.addValue("toDate", toDate);
        boolean addAgentCondition = false;
        if (mismatchingAgents != null && mismatchingAgents.size() > 0) {
            paramSource.addValue("agentCodes", mismatchingAgents);
            addAgentCondition = true;
        }
        String sql = "SELECT AT.PNR AT_PNR, AT.AMT AT_AMT, PT.PNR PT_PNR, PT.AMT PT_AMT, " +
                " nvl(at.amt,0)+nvl(pt.amt,0) DIFF FROM  " +
                "(SELECT PNR, SUM(AGT.amount) as AMT FROM " + opSchema + "T_AGENT_TRANSACTION AGT " +
                "WHERE AGT.tnx_date BETWEEN to_date(:fromDate,'dd-MON-yyyy HH24:MI:ss')  " +
                "and to_date(:toDate,'dd-MON-yyyy HH24:MI:ss') " +
                "AND AGT.nominal_code IN (1,7) " +
//                " AND AGT.pnr in ('11444439') " + //FIXME TOBE REMOVED
//                " AND AGT.pnr in ('11218796','11301453','11333525','11425628','11428337','11433123','11434587','11435106','11437682','11438520') " + //FIXME TOBE REMOVED
                "AND AGT.agent_code IN (SELECT AGENT_CODE FROM " + opSchema + "T_AGENT AG " +
                " WHERE (AG.is_lcc_integration_agent = 'N' OR AG.is_lcc_integration_agent IS NULL)  " +
                (addAgentCondition ? " AND AGT.agent_code IN (:agentCodes) " : "") +
                " AND AG.airline_code = (select PARAM_VALUE from " + opSchema + "t_app_parameter where param_KEY='AIRLINE_71')) " +
                " group by AGT.PNR) AT, " +
                "(SELECT  PNR,SUM(AMT) AS AMT from " +
                "    (SELECT P.PNR, A.AMOUNT AMT FROM " + opSchema + "T_PAX_TRANSACTION A, " +
                opSchema + "T_AGENT G, " + opSchema + "T_PNR_PASSENGER P " +
                "      WHERE A.NOMINAL_CODE IN ( '19','25' ) " +
                "    AND A.TNX_DATE BETWEEN to_date(:fromDate,'dd-MON-yyyy HH24:MI:ss') " +
                " and to_date(:toDate,'dd-MON-yyyy HH24:MI:ss')  " +
//                " AND P.pnr in ('11444439') " + //FIXME TO BE REMOVED
//                " AND P.pnr in ('11218796','11301453','11333525','11425628','11428337','11433123','11434587','11435106','11437682','11438520') " + //FIXME TO BE REMOVED
                "    AND A.AGENT_CODE                = G.AGENT_CODE(+) " +
                "    AND (A.PAYMENT_CARRIER_CODE     = (select PARAM_VALUE from " + opSchema + "t_app_parameter where" +
                " param_KEY='AIRLINE_13') OR A.PAYMENT_CARRIER_CODE      IS NULL ) " +
                "    AND (G.IS_LCC_INTEGRATION_AGENT = 'N' OR G.IS_LCC_INTEGRATION_AGENT  IS NULL)  " +
                (addAgentCondition ? " AND A.agent_code IN (:agentCodes) " : "") +
                "    AND P.PNR_PAX_ID = A.PNR_PAX_ID " +
                "      UNION ALL      " +
                "     SELECT P.PNR, A.AMOUNT AMT FROM " + opSchema + "T_PAX_EXT_CARRIER_TRANSACTIONS A, " +
                opSchema + "T_PNR_PASSENGER P " +
                "      WHERE A.NOMINAL_CODE IN ('19','25') " +
                (addAgentCondition ? " AND A.agent_code IN (:agentCodes) " : "") +
//                " AND P.pnr in ('11444439') " + //FIXME TO BE REMOVED
//                " AND P.pnr in ('11218796','11301453','11333525','11425628','11428337','11433123','11434587','11435106','11437682','11438520') " + //FIXME TO BE REMOVED
                "    AND A.TXN_TIMESTAMP BETWEEN to_date(:fromDate,'dd-MON-yyyy HH24:MI:ss') " +
                " and to_date(:toDate,'dd-MON-yyyy HH24:MI:ss') " +
                "    AND A.PNR_PAX_ID = P.PNR_PAX_ID " +
                "    ) " +
                "    group by PNR ) PT " +
                "where PT.PNR = AT.PNR(+) and (nvl(at.amt,0)+nvl(pt.amt,0))<> 0 ";

        logQuery(sql, paramSource);
        return jdbcTemplate.query(sql, paramSource, new RowMapper<AgentTnxPaxTnxDTO>() {
            @Override
            public AgentTnxPaxTnxDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
                AgentTnxPaxTnxDTO data = new AgentTnxPaxTnxDTO(tuple.opCarrier);
                data.setAtPNR(rs.getString("AT_PNR"));
                data.setAtAmount(rs.getBigDecimal("AT_AMT"));
                data.setPtPNR(rs.getString("PT_PNR"));
                data.setPtAmount(rs.getBigDecimal("PT_AMT"));
                data.setDifference(rs.getBigDecimal("DIFF"));
                return data;
            }
        });
    }

    public List<String> getPNRsForMissingPaxTransactions(String fromDate, String toDate, CarrierTuple tuple,
                                                         List<String> mismatchingAgents) {
        MapSqlParameterSource paramSource = new MapSqlParameterSource();
        String opSchema = getSchema(tuple.opCarrier);

        paramSource.addValue("fromDate", fromDate);
        paramSource.addValue("toDate", toDate);
        boolean addAgentCondition = false;
        if (mismatchingAgents != null && mismatchingAgents.size() > 0) {
            paramSource.addValue("agentCodes", mismatchingAgents);
            addAgentCondition = true;
        }
        String sql =
                "(SELECT DISTINCT AGT.PNR FROM " + opSchema + "T_AGENT_TRANSACTION AGT " +
                        "WHERE AGT.tnx_date BETWEEN to_date(:fromDate,'dd-MON-yyyy HH24:MI:ss')  " +
                        "and to_date(:toDate,'dd-MON-yyyy HH24:MI:ss') " +
                        "AND AGT.nominal_code IN (1,7) " +
//                " AND AGT.pnr in ('11444439') " + //FIXME TOBE REMOVED
//                " AND AGT.pnr in ('11218796','11301453','11333525','11425628','11428337','11433123','11434587','11435106','11437682','11438520') " + //FIXME TOBE REMOVED
                        "AND AGT.agent_code IN (SELECT AGENT_CODE FROM " + opSchema + "T_AGENT AG " +
                        " WHERE (AG.is_lcc_integration_agent = 'N' OR AG.is_lcc_integration_agent IS NULL)  " +
                        (addAgentCondition ? " AND AGT.agent_code IN (:agentCodes) " : "") +
                        " AND AG.airline_code = (select PARAM_VALUE from " + opSchema + "t_app_parameter where param_KEY='AIRLINE_71')) ) " +
                        " MINUS " +
                        "(SELECT  DISTINCT PNR  from " +
                        "    (SELECT P.PNR FROM " + opSchema + "T_PAX_TRANSACTION A, " +
                        opSchema + "T_AGENT G, " + opSchema + "T_PNR_PASSENGER P " +
                        "      WHERE A.NOMINAL_CODE IN ( '19','25' ) " +
                        "    AND A.TNX_DATE BETWEEN to_date(:fromDate,'dd-MON-yyyy HH24:MI:ss') " +
                        " and to_date(:toDate,'dd-MON-yyyy HH24:MI:ss')  " +
//                " AND P.pnr in ('11444439') " + //FIXME TO BE REMOVED
//                " AND P.pnr in ('11218796','11301453','11333525','11425628','11428337','11433123','11434587','11435106','11437682','11438520') " + //FIXME TO BE REMOVED
                        "    AND A.AGENT_CODE                = G.AGENT_CODE(+) " +
                        "    AND (A.PAYMENT_CARRIER_CODE     = (select PARAM_VALUE from " + opSchema + "t_app_parameter where" +
                        " param_KEY='AIRLINE_13') OR A.PAYMENT_CARRIER_CODE      IS NULL ) " +
                        "    AND (G.IS_LCC_INTEGRATION_AGENT = 'N' OR G.IS_LCC_INTEGRATION_AGENT  IS NULL)  " +
                        (addAgentCondition ? " AND A.agent_code IN (:agentCodes) " : "") +
                        "    AND P.PNR_PAX_ID = A.PNR_PAX_ID " +
                        "      UNION       " +
                        "     SELECT DISTINCT P.PNR FROM " + opSchema + "T_PAX_EXT_CARRIER_TRANSACTIONS A, " +
                        opSchema + "T_PNR_PASSENGER P " +
                        "      WHERE A.NOMINAL_CODE IN ('19','25') " +
                        (addAgentCondition ? " AND A.agent_code IN (:agentCodes) " : "") +
//                " AND P.pnr in ('11444439') " + //FIXME TO BE REMOVED
//                " AND P.pnr in ('11218796','11301453','11333525','11425628','11428337','11433123','11434587','11435106','11437682','11438520') " + //FIXME TO BE REMOVED
                        "    AND A.TXN_TIMESTAMP BETWEEN to_date(:fromDate,'dd-MON-yyyy HH24:MI:ss') " +
                        " and to_date(:toDate,'dd-MON-yyyy HH24:MI:ss') " +
                        "    AND A.PNR_PAX_ID = P.PNR_PAX_ID " +
                        "    ) " +
                        "     )  ";
        logQuery(sql, paramSource);
        return jdbcTemplate.query(sql, paramSource, (rs, rowNum) -> {
            return rs.getString("PNR");
        });
    }

    public Map<String, ParentPNRDTO> getSplittedPNRs(Set<String> pnrs, String fromDate, String toDate, CarrierTuple tuple) {
        MapSqlParameterSource paramSource = new MapSqlParameterSource();
        String opSchema = getSchema(tuple.opCarrier);
        paramSource.addValue("pnrs", pnrs);
        paramSource.addValue("fromDate", fromDate);
//        paramSource.addValue("toDate", toDate);
        String sql = "select * from " + opSchema + "t_reservation_audit where " +
                "template_code in ( 'SPTRES', 'NEWRMP') and PNR in (:pnrs) and" +
                " timestamp >= " +
                " to_date(:fromDate,'dd-MON-yyyy HH24:MI:ss')  ";
//                " timestamp between " +
//                " to_date(:fromDate,'dd-MON-yyyy HH24:MI:ss') " +
//                "                 and to_date(:toDate,'dd-MON-yyyy HH24:MI:ss') ";
        logQuery(sql, paramSource);
        Map<String, ParentPNRDTO> results = new HashMap<>();
        jdbcTemplate.query(sql, paramSource, (rs, rowNum) -> {
            ParentPNRDTO pnrDTO = new ParentPNRDTO();
            pnrDTO.setPnr(rs.getString("PNR"));
            pnrDTO.setSplitDate(rs.getDate("TIMESTAMP"));
            pnrDTO.addCarrierCodes(tuple.opCarrier);
            results.put(pnrDTO.getPnr(), pnrDTO);
            return pnrDTO;
        });
        return results;
    }

    public Map<String, Set<SplitPNRDTO>> getNewPNRsForSplitted(Set<String> pnrs, String fromDate, String toDate, CarrierTuple tuple) {
        MapSqlParameterSource paramSource = new MapSqlParameterSource();
        String opSchema = getSchema(tuple.opCarrier);
        paramSource.addValue("pnrs", pnrs);
        paramSource.addValue("fromDate", fromDate);
        paramSource.addValue("toDate", toDate);
        String sql = "select * from " + opSchema + "t_reservation_audit where " +
                "template_code in ('SPTRES','NEWRMP') and PNR in (:pnrs) and" +
                " timestamp >= " +
                " to_date(:fromDate,'dd-MON-yyyy HH24:MI:ss') ";
//                " timestamp between " +
//                " to_date(:fromDate,'dd-MON-yyyy HH24:MI:ss') " +
//                "                 and to_date(:toDate,'dd-MON-yyyy HH24:MI:ss') ";
        logQuery(sql, paramSource);
        Map<String, Set<SplitPNRDTO>> relatedPNRs = new HashMap<>();
        jdbcTemplate.query(sql, paramSource, (rs, rowNum) -> {
            //New PNR : 69287105 Split passenger(s):  , T B A, T B A - ADULT,  , T B A, T B A - ADULT,  , T B A, T B A - ADULT,  , T B A, T B A - ADULT,  , T B A, T B A - ADULT, .  Connected IP adddress is 123.253.66.119 Origin Carrier Code is G9.  User is G9/[ABYDAC906]/[ABYNTTDAC20].
            String content = rs.getString("CONTENT");
            String pnr = rs.getString("PNR");
            String childPNR = content.split("\\s+")[3];

            if (!relatedPNRs.containsKey(pnr)) {
                relatedPNRs.put(pnr, new HashSet<>());
            }
            SplitPNRDTO splitPNR = new SplitPNRDTO();
            splitPNR.setChildPNR(childPNR);
            splitPNR.setParentPNR(pnr);
            splitPNR.setCarrierCode(tuple.opCarrier);
            splitPNR.setSplitDate(rs.getDate("timestamp"));
            relatedPNRs.get(pnr).add(splitPNR);
            return content;
        });
        return relatedPNRs;
    }
}
